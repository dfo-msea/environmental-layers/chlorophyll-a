# Surface chlorophyll a concentration from satellite observations

__Main author:__  Jessica Nephin  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@gmail.com | tel: 250-363-6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
  + [Download data](#download-data)
  + [Daily swath rasters](#daily-swath-rasters)
  + [Monthly mosaics](#monthly-mosaics)
  + [Interpolate](#interpolate)
  + [Bloom Frequency](#bloom-frequency)
  + [Mean Chlorophyll a](#mean-chlorophyll-a)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To produce a surface chlorophyll a concentration raster layer to be used as a predictor in species distribution models.


## Summary
This project was created to automated the process of transforming the MODIS L2 (1 x 1 km) Chlor_a (mg m^-3) band into raster layers depicting the spatial distribution of surface chlorophyll a concentration within the extent of Canadian’s Pacific EEZ. Mean chlorophyll a concentration and bloom frequency layers were derived from the satellite observations to identify productivity hotspots. The variables were derived from observations between March and October over a 4 year period from 2012 to 2015. November to February were excluded because of limited data due to increased cloud cover.

## Status
Completed

## Contents
This project contains a collection of R, Python and shell scripts and xml files all used to create the chlorophyll raster layers. See methods for instructions on the order the scripts should be run and the operations they perform.


## Methods
### Download data
* Download full res (1km) daily swath data (L2) from aqua MODIS from 2012 to 2015 between March and October. Data source: http://oceandata.sci.gsfc.nasa.gov
* Only download daily swaths between 19:00 and 24:00 UTC when aqua MODIS is capturing data from the BC region.
* Delete daily swaths if they do not include any data in the region of interest (BC EEZ).
* Run Chl-Download.R from cygwin environment

### Daily swath rasters
* Re-projection and quality flag masking is carried out using the command line tool gpt from Sentinel Application Platform (SNAP) which can be found here http://step.esa.int/main/toolboxes/snap/
* Chlor_a (mg m^-3) band was re-projected to BC Albers projected coordinate system (EPSG:3005).
* Two different chl-a layers were produced.
    1.  a 'maskall' layer where all data quality flags that are masked by default in L3 products are masked (LAND, ATMFAIL, HIGLINT, HILT, HISATZEN, CLDICE, COCCOLITH, HISOLZEN, LOWLW,NAVFAIL, MAXAERITER, ABSAER and STRAYLIGHT).
    2.  a 'straylight' layer where all data quality flags that are masked by default in L3 products except for STRAYLIGHT are masked. Not masking the STRAYLIGHT flag retains more data in the coastal region but as a result the chl-a values in those areas is more uncertain.

### Monthly mosaics
* Projected and masked daily swath chl-a was mosaicked by month using SNAP's command line tool gpt.
* Mosaicking was performed using bilinear interpolation at the native resolution 1 x 1 km.
* When daily swaths were overlapping during mosaicking the mean value was returned.

### Interpolate
* The monthly chl-a rasters of 1 x 1 km cell size (its native resolution) were interpolated spatially using Spline with Barriers (ArcGIS 10.4) to fill in any gaps in the monthly rasters that remained after mosaicking. These gaps were typically located nearshore and in coastal inlets.
* Raster interpolation was performed in ArcMap using Spline with Barrier algorithm (SplineBarriers.py) using a smoothed coastline.
* Both products, 1) 'maskall' and 2) 'straylight', were interpolated.

### Bloom Frequency
* Monthly bloom frequency was calculated (from the monthly interpolated chl-a) in R using the raster package.
* Cells with Chlor_a ≥ 3.0 mg m^-3 were reclassified as blooming. For each month, each cell was classified as either blooming (== 1) or not (== 0). This method was previously used by Gregr et al. (2016).
* The 3 mg m^-3 is a chlorophyll bloom threshold reported for the study area (Mackas et al. 2007).
* Monthly bloom rasters were added together to calculate bloom frequency which ranged from 0 to 32. Values of 32 represent regions where monthly chl-a concentrations exceeded the bloom threshold during all months from March to October over a 4 year period from 2012 to 2015.

### Mean Chlorophyll a
* Mean chl-a (from the monthly interpolated chl-a) was calculated in R using the raster package.
* The mean was calculated from all monthly rasters from March to October over a 4 year period from 2012 to 2015.


## Requirements
* R, Python, GDAL, and the Sentinel Application Platform (SNAP) which can be found here http://step.esa.int/main/toolboxes/snap/
* The arcpy Python module which requires an ESRI ArcMap and Spatial Analyst license
* A polygon of the BC EEZ area for clipping raster layers


## Caveats
Raster layers are more uncertain in the coastal nearshore areas where cloud cover is greater and thus data gaps were present that required some spatial interpolation.


## Uncertainty
* The uncertainty layer represents the extent to which the mean chl-a and bloom frequency layers were extrapolated.
* Uncertainty values represent the frequency (0 to 32) of no data values in the monthly rasters before interpolation was completed. A value of 32 indicates that no data values were present for that cell for the 32 months examined. A value of 0 indicates that there was a chla-a value for that cell for all 32 months examined.
* The uncertainty layers for both 'maskall' and 'straylight' products, helps visualize the differences between the two layers.
* To reduce uncertainty, the final bloom frequency and mean chl-a layers were constrained by converting cells to 'no data' where the uncertainty raster was equal to 32 (a raster cell no data for all 32 months). These layers were suffixed with 'noextrap'.


## Acknowledgements
Ed Gregr, Joanne Lessard


## References
Gregr, E.J., Gryba, R., Li, M.Z., Alidina, H., Kostylev, V., and Hannah, C.G. 2016. A benthic habitat template for Pacific Canada’s continental shelf. Can. Tech. Rep. Hydrogr. Ocean Sci. 312: vii + 37 p.

Mackas, D., Peña, A., Johannessen, D., Birch, R., Borg, K., and Fissel, D. 2007. Appendix D: Plankton. Pages iv + 33 p. in Lucas BG, Verrin S, Brown R, eds. Ecosystem overview: Pacific North Coast Integrated Management Area (PNCIMA), vol. 2667 Can. Tech. Rep. Fish. Aquat. Sci.
